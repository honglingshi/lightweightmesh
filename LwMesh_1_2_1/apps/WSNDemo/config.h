/**
 * \file config.h
 *
 * \brief WSNDemo application and stack configuration
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 * $Id: config.h 9267 2014-03-18 21:46:19Z ataradov $
 *
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

/*- Definitions ------------------------------------------------------------*/
#define APP_ADDR                0//0x8888

#define APP_PANID               0x1234
#define APP_SECURITY_KEY        "TestSecurityKey0"

#ifdef PHY_AT86RF212
// #define APP_CHANNEL           0x01
// #define APP_BAND              0x00
// #define APP_MODULATION        0x24
#else
#define APP_CHANNEL           21
#endif

#if APP_ADDR == 0
#define APP_CAPTION           "Coord_For_uSu_Air"
// #define APP_CAPTION           "Coord_N38_HWTest"
#define APP_COORDINATOR
#define APP_SENDING_INTERVAL  60000
#elif APP_ADDR < 0x8000
#define APP_CAPTION           "HWT_Edu_0100"
#define APP_ROUTER
#define APP_SENDING_INTERVAL  50
#else
//#define APP_CAPTION           "End_N58_GZV3"
// #define APP_CAPTION           "Edu_0145_O2"
#define APP_CAPTION           "End_For_GaoHui"
#define APP_ENDDEVICE
#define APP_SENDING_INTERVAL  1000
#endif

#define HAL_UART_CHANNEL         1
#define HAL_UART_RX_FIFO_SIZE    20
#define HAL_UART_TX_FIFO_SIZE    1000

#define HAL_Uart0_CHANNEL        0
#define HAL_Uart0_RX_FIFO_SIZE   25
#define HAL_Uart0_TX_FIFO_SIZE   1000

//#define HAL_Uart0_Actived

//#define _RS485_SENSOR_
//#define _COZIR_CO2_SENSOR_
//#define _SHARP_PM25_SENSOR_
//#define _YW51_PM25_SENSOR_
#define _SENSOR_LM75_
//#define _SENSOR_SHT1X_
//#define ALPHASENSE_END

//#define _SENSOR_DHT22_

//#define _SENSOR_DS18B20_

//#define LORA_433MHZ
//#define LORA_ON_IOL

//#define RF_ON_OFF_ARDIO
//#define POWER_ON_OFF_ARDIO
#define FIX_ON_OFF_ARDIO
#define ACTIVE_LED
//#define RGB_LCD
//#define RGB_LCD_TXNUM
//#define RGB_LCD_RXDATA

//#define NEO_Pixel

//#define PHY_ENABLE_RANDOM_NUMBER_GENERATOR

#define SYS_SECURITY_MODE                   0

#define NWK_BUFFERS_AMOUNT                  30
#define NWK_DUPLICATE_REJECTION_TABLE_SIZE  50
#define NWK_DUPLICATE_REJECTION_TTL         2000 // ms
#define NWK_ROUTE_TABLE_SIZE                100
#define NWK_ROUTE_DEFAULT_SCORE             3
#define NWK_ACK_WAIT_TIME                   1000 // ms
#define NWK_GROUPS_AMOUNT                   3
#define NWK_ROUTE_DISCOVERY_TABLE_SIZE      5
#define NWK_ROUTE_DISCOVERY_TIMEOUT         1000 // ms

#define NWK_ENABLE_ROUTING
//#define NWK_ENABLE_SECURITY
#define NWK_ENABLE_ROUTE_DISCOVERY

#endif // _CONFIG_H_
