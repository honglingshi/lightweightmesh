/*! \file

\brief iLive2 Light sensor and battery module interface

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

#ifndef _HAL_ADC_H_
#define _HAL_ADC_H_


enum
{
    ADC_BATTERY_CHANNEL = 0,
    ADC_LIGHT_CHANNEL
};

/*- Prototypes ---------------------------------------------------------------*/

void sensor_power_on(void);
void sensor_power_off(void);

void adc_init(void);
uint16_t adc_read(uint8_t ch);
void adc_close(void);

#endif