/*! \file

\brief Temperature sensor module interface

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

#ifndef _HAL_LM75_H_
#define _HAL_LM75_H_

void lm75_init(void);
short lm75_read_temperature(void);

#endif //_HAL_LM75_H_
