/*! \file

\brief HAL LCD module implementation

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

#include "config.h"
#include "Wire.h"
#include "rgb_lcd.h"
#include "halLCD.h"

rgb_lcd lcd;

const int colorR = 0;
const int colorG = 0;
const int colorB = 255;

#ifdef LORA_433MHZ
extern unsigned char   PKTRSSIVALUE;
#endif // LORA_433MHZ

#ifdef __cplusplus
extern "C" {
#endif

void halLCD_Init()
{
    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);

    lcd.setRGB(colorR, colorG, colorB);

    // Print a message to the LCD.
#ifdef RGB_LCD_RXDATA
    lcd.print("uSu-Edu LoRa RX");
#endif // RGB_LCD_TXNUM

#ifdef RGB_LCD_TXNUM
    lcd.print("uSu-Edu LoRa TX");
#endif // RGB_LCD_TXNUM
}

void halLCD_Print(int nNum)
{
    // set the cursor to column 0, line 1
    // (note: line 1 is the second row, since counting begins with 0):
    lcd.setCursor(0, 1);
    lcd.print("                ");
    // print the number of seconds since reset:
    lcd.setCursor(0, 1);
    lcd.print(nNum);
}

void halLCD_PrintHex(unsigned char Buf[], int nNum)
{
    // set the cursor to column 0, line 1
    // (note: line 1 is the second row, since counting begins with 0):
    lcd.setCursor(0, 1);
    lcd.print("                ");
    // print the Hex:
    lcd.setCursor(0, 1);
    for(int i=0; i<nNum && i<8; i++)
    {
        if(Buf[i]< 16)
            lcd.print("0");
        lcd.print(Buf[i], HEX);
    }
}

#ifdef LORA_433MHZ
void halLCD_PrintLoRaFrame(unsigned char Buf[], int nNum)
{
    unsigned long nRxIndex;
    int nRSSI;

    (void)(nNum); // Unused

    // set the cursor to column 0, line 1
    // (note: line 1 is the second row, since counting begins with 0):
    lcd.setCursor(0, 1);
    lcd.print("                ");

    nRxIndex = (((unsigned long)Buf[1]) << 24)
                | (((unsigned long)Buf[2]) << 16)
                | (((unsigned long)Buf[3]) << 8)
                | (((unsigned long)Buf[4])) ;
    // print the Hex:
    lcd.setCursor(0, 1);
    lcd.print(nRxIndex);

    nRSSI = PKTRSSIVALUE;
    nRSSI -= 164;
    lcd.print(" RSSI:");
    lcd.print(nRSSI);
}
#endif // LORA_433MHZ

#ifdef __cplusplus
}
#endif
