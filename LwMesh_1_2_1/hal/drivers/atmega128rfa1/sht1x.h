#ifndef _SCM_SHT1X_H_
#define _SCM_SHT1X_H_

void sht1x_init(void);

unsigned char sht1x_readTemperature(unsigned char* pmsb, unsigned char* plsb, unsigned char* pchksum);

unsigned char sht1x_readHumidity(unsigned char* pmsb, unsigned char* plsb, unsigned char* pchksum);

unsigned char sht1x_softReset();

void delayms(unsigned int d);

#endif //_SCM_SHT1X_H_

