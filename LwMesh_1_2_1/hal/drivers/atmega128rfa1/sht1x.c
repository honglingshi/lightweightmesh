#include "halI2c.h"
#include "twi.h"
#include "sht1x.h"
#include "halSleep.h"

#if 1//def SCM_SHT1X

#define SHT_1X_READ_TEMPERATURE		0x3
#define SHT_1X_READ_HUMIDITY  		0x5
#define SHT_1X_READ_STATUS  		0x7
#define SHT_1X_WRITE_STATUS  		0x6
#define SHT_1X_SOFT_RESET			0x1E

/*
#define DELAY_CNT 0x200

void delayms(unsigned int d)
{
    volatile unsigned int i,j;
    for(j=0; j < DELAY_CNT; j++)
    {
        for (i = 0; i<d; i++)
        {
            ;
        }
    }
}
*/

void sht1x_init(void)
{
//	printf("sht1x_init\r\n");

#ifdef GPIO_SIM_I2C
	i2c_init();
#else
    halI2C_Init();
#endif
}

#ifdef GPIO_SIM_I2C

void sht1x_i2c_start(void)
{
	 /* release two wires */
	Set_SDA;
	Set_SCL;
	_i2c_delay();

	/* pull down the SDA */
	Clr_SDA;
	_i2c_delay();

	Clr_SCL;
	_i2c_delay();

	Set_SCL;
	_i2c_delay();

	Set_SDA;
	_i2c_delay();

	Clr_SCL;
	_i2c_delay();
}

void sht1x_i2c_connect_reset(void)
{
	unsigned char i;
//	volatile unsigned char d = 0;

	Set_SDA;
	_i2c_delay();

	for(i=0; i<10; i++)
	{
		/* emit the SDA via trigger a square-wave on SCL */
		Set_SCL;
		_i2c_delay();
		Clr_SCL;
		_i2c_delay();
	}

	sht1x_i2c_start();
}

unsigned char sht1x_readTemperature(unsigned char* pmsb, unsigned char* plsb, unsigned char* pchksum)
{
	sht1x_i2c_connect_reset();
	i2c_tx(SHT_1X_READ_TEMPERATURE);	//Send Cmd
//	delayms(300);
	delayms(55+11);
	*pmsb = i2c_rx(1); 					//Send Cmd
	*plsb = i2c_rx(1); 					//Send Cmd
	*pchksum = i2c_rx(0); 				//Send Cmd
	i2c_stop(); 						//End
	return 1;
}

unsigned char sht1x_readHumidity(unsigned char* pmsb, unsigned char* plsb, unsigned char* pchksum)
{
	sht1x_i2c_connect_reset();
	i2c_tx(SHT_1X_READ_HUMIDITY);		//Send Cmd
//	delayms(300);
	delayms(11+2);
	*pmsb = i2c_rx(1); 					//Send Cmd
	*plsb = i2c_rx(1); 					//Send Cmd
	*pchksum = i2c_rx(0); 				//Send Cmd
	i2c_stop(); 						//End
	return 1;
}

unsigned char sht1x_readStatus(unsigned char* pstatus, unsigned char* pchksum)
{
	sht1x_i2c_connect_reset();
	i2c_tx(SHT_1X_READ_STATUS);			//Send Cmd
	*pstatus = i2c_rx(1); 				//Send Cmd
	*pchksum = i2c_rx(0); 				//Send Cmd
	i2c_stop(); 						//End
	return 1;
}

unsigned char sht1x_writeStatus(unsigned char status)
{
	sht1x_i2c_connect_reset();
	i2c_tx(SHT_1X_WRITE_STATUS);		//Send Cmd
	i2c_tx(status);					    //Send Cmd
	i2c_stop(); 						//End
	return 1;
}

unsigned char sht1x_softReset()
{
	sht1x_i2c_connect_reset();
	i2c_tx(SHT_1X_SOFT_RESET);			//Send Cmd
	i2c_stop(); 						//End
	delayms(11+2);

	sht1x_writeStatus(0x01);	        //	8bit RH / 12bit Temperature

	return 1;
}
#endif

#endif
