/*! \file

\brief HAL I2C module implementation

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

#include "Wire.h"
#include "halI2c.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "halSleep.h"

#ifdef USU_GPIO_I2C
    #include "halGpioI2c.h"
#elif defined(USU_TWI_I2C)
#endif


void halI2C_Init()
{
#ifdef USU_GPIO_I2C
#elif defined(USU_TWI_I2C)
    Wire.begin();
#endif
}

/*******************************************************************************
* Function Name     : ReadReg
* Description       : Generic Reading function. It must be fullfilled with either
*           : I2C or SPI reading functions
* Input         : Register Address
* Output        : Data REad
* Return        : None
*******************************************************************************/
uint8_t halI2C_ReadReg(uint8_t deviceAddr, uint8_t Reg, uint8_t *Data)
{
    halI2C_write_read_Data(deviceAddr, &Reg, Data, 1, 1, 1, 100);

    return 1;
}


/*******************************************************************************
* Function Name     : WriteReg
* Description       : Generic Writing function. It must be fullfilled with either
*           : I2C or SPI writing function
* Input         : Register Address, Data to be written
* Output        : None
* Return        : None
*******************************************************************************/
uint8_t halI2C_WriteReg(uint8_t deviceAddress, uint8_t WriteAddr, uint8_t Data)
{
    //To be completed with either I2c or SPI writing function
    //SPI_Mems_Write_Reg(Reg, Data);
    //I2C_ByteWrite(&Data,  deviceAddress,  WriteAddr);
#ifdef USU_GPIO_I2C
    i2c_write_reg(deviceAddress, WriteAddr, Data);
#elif defined(USU_TWI_I2C)
    uint8_t dta[2] = {WriteAddr, Data};
    Wire.beginTransmission(deviceAddress);               // transmit to device #4
    Wire.write(dta, 2);                                    // sends two byte
    Wire.endTransmission();                             // stop transmitting
#endif
    return 1;
}


uint8_t halI2C_WriteData(uint8_t deviceAddress, uint8_t *pData, uint8_t len)
{
    //To be completed with either I2c or SPI writing function
    //SPI_Mems_Write_Reg(Reg, Data);
    //I2C_ByteWrite(&Data,  deviceAddress,  WriteAddr);
#ifdef USU_GPIO_I2C
    i2c_write_string(deviceAddress, pData, len);
#elif defined(USU_TWI_I2C)
    Wire.beginTransmission(deviceAddress);               // transmit to device #4
    Wire.write(pData, len);                                    // sends two byte
    Wire.endTransmission();                             // stop transmitting
#endif
    return 1;
}

char halI2C_ReadData(unsigned char i2cAddress, unsigned char *buffer, unsigned char out_cnt, uint16_t period2)
{
    uint16_t timeout = 0;
    //get response
    Wire.requestFrom(i2cAddress, (uint8_t)out_cnt);    // request 4 bytes from slave device
    while(Wire.available() == 0)
    {
        if(timeout++ > period2)
            return -2;//time out
        delayms(1);
    }
    if(Wire.available() != out_cnt)
        return -3;//rtnData length wrong
    for(uint8_t i=0; i<out_cnt; i++)
    {
        buffer[i] = Wire.read();
    }

    return 1;
}

char halI2C_write_read_Data(unsigned char i2cAddress, unsigned char *cmd, unsigned char *buffer, unsigned char in_cnt, unsigned char out_cnt, uint16_t period1, uint16_t period2)
{
#ifdef USU_GPIO_I2C
    (void)period1;
    (void)period2;

    i2c_write_read_string(i2cAddress, cmd, buffer, in_cnt, out_cnt);
#elif defined(USU_TWI_I2C)
    uint16_t timeout = 0;
    //send command
    halI2C_WriteData(i2cAddress, cmd, in_cnt);
    //wait for a while
    if(period1)
        delayms(period1);
    //get response
    Wire.requestFrom(i2cAddress, (uint8_t)out_cnt);    // request 4 bytes from slave device
    while(Wire.available() == 0)
    {
        if(timeout++ > period2)
            return -2;//time out
        delayms(1);
    }
    if(Wire.available() != out_cnt)
        return -3;//rtnData length wrong
    for(uint8_t i=0; i<out_cnt; i++)
    {
        buffer[i] = Wire.read();
    }
#endif
    return 1;
}

#ifdef __cplusplus
}
#endif
