#include "halI2c.h"
#include "lsm303dlhc_driver.h"
#include "LSM330DLC.h"
#include "halSleep.h"

// ************************************************************************************************************
// Start Of I2C Gyroscope and Accelerometer LSM330
// ************************************************************************************************************

////////////////////////////////////
//           ACC start            //
////////////////////////////////////
void LSM330_ACC_init ()
{

    delayms(10);
    //WriteReg(LSM330_ACC_ADDRESS ,0x20 ,0x17 ); // 1Hz
    //WriteReg(LSM330_ACC_ADDRESS ,0x20 ,0x27 ); // 10Hz
    WriteReg(LSM330_ACC_ADDRESS , 0x20 , 0x37 ); // 25Hz
    //WriteReg(LSM330_ACC_ADDRESS ,0x20 ,0x47 ); // 50Hz
    //WriteReg(LSM330_ACC_ADDRESS ,0x20 ,0x57 ); // 100Hz

    delayms(5);
    WriteReg(LSM330_ACC_ADDRESS ,0x23 ,0x08 ); // 2G
    //WriteReg(LSM330_ACC_ADDRESS ,0x23 ,0x18 ); // 4G
    //WriteReg(LSM330_ACC_ADDRESS , 0x23 , 0x28 ); // 8G
    //WriteReg(LSM330_ACC_ADDRESS ,0x23 ,0x38 ); // 16G

    delayms(5);
    WriteReg(LSM330_ACC_ADDRESS, 0x21, 0x00); // no high-pass filter
}

//#define ACC_DELIMITER 5 // for 2g
#define ACC_DELIMITER 4 // for 4g
//#define ACC_DELIMITER 3 // for 8g
//#define ACC_DELIMITER 2 // for 16g

/*
  void ACC_getADC () {
  i2c_getSixRawADC(LSM330_ACC_ADDRESS,0x80|0x28);// Start multiple read at reg 0x28

  ACC_ORIENTATION( ((rawADC[1]<<8) | rawADC[0])>>ACC_DELIMITER ,
                   ((rawADC[3]<<8) | rawADC[2])>>ACC_DELIMITER ,
                   ((rawADC[5]<<8) | rawADC[4])>>ACC_DELIMITER );
  ACC_Common();
}
*/

/*******************************************************************************
* Function Name  : GetAccAxesRaw
* Description    : Read the Acceleration Values Output Registers
* Input          : buffer to empity by AccAxesRaw_t Typedef
* Output         : None
* Return         : Status [MEMS_ERROR, MEMS_SUCCESS]
*******************************************************************************/
status_t LSM330_GetAccAxesRaw(AccAxesRaw_t *buff)
{
    u8_t valueL;
    u8_t valueH;

    if ( !ReadReg(LSM330_ACC_ADDRESS, LSM330_OUT_X_L, &valueL) )
    {
        return MEMS_ERROR;
    }

    if ( !ReadReg(LSM330_ACC_ADDRESS, LSM330_OUT_X_H, &valueH) )
    {
        return MEMS_ERROR;
    }

    buff->AXIS_X = (i16_t)( (valueH << 8) | valueL ) / 16;

    if ( !ReadReg(LSM330_ACC_ADDRESS, LSM330_OUT_Y_L, &valueL) )
    {
        return MEMS_ERROR;
    }

    if ( !ReadReg(LSM330_ACC_ADDRESS, LSM330_OUT_Y_H, &valueH) )
    {
        return MEMS_ERROR;
    }

    buff->AXIS_Y = (i16_t)( (valueH << 8) | valueL ) / 16;

    if ( !ReadReg(LSM330_ACC_ADDRESS, LSM330_OUT_Z_L, &valueL) )
    {
        return MEMS_ERROR;
    }

    if ( !ReadReg(LSM330_ACC_ADDRESS, LSM330_OUT_Z_H, &valueH) )
    {
        return MEMS_ERROR;
    }

    buff->AXIS_Z = (i16_t)( (valueH << 8) | valueL ) / 16;

    return MEMS_SUCCESS;
}


/*******************************************************************************
* Function Name  : LSM330_GetGyroAxesRaw
* Description    : Read the Gyroscope Values Output Registers
* Input          : buffer to empity by GyroAxesRaw_t Typedef
* Output         : None
* Return         : Status [MEMS_ERROR, MEMS_SUCCESS]
*******************************************************************************/
status_t LSM330_GetGyroAxesRaw(GyroAxesRaw_t *buff)
{
    u8_t valueL;
    u8_t valueH;

    if ( !ReadReg(LSM330_GYRO_ADDRESS, LSM330_OUT_X_L, &valueL) )
    {
        return MEMS_ERROR;
    }

    if ( !ReadReg(LSM330_GYRO_ADDRESS, LSM330_OUT_X_H, &valueH) )
    {
        return MEMS_ERROR;
    }

    buff->AXIS_X = (i16_t)( (valueH << 8) | valueL ) / 16;

    if ( !ReadReg(LSM330_GYRO_ADDRESS, LSM330_OUT_Y_L, &valueL) )
    {
        return MEMS_ERROR;
    }

    if ( !ReadReg(LSM330_GYRO_ADDRESS, LSM330_OUT_Y_H, &valueH) )
    {
        return MEMS_ERROR;
    }

    buff->AXIS_Y = (i16_t)( (valueH << 8) | valueL ) / 16;

    if ( !ReadReg(LSM330_GYRO_ADDRESS, LSM330_OUT_Z_L, &valueL) )
    {
        return MEMS_ERROR;
    }

    if ( !ReadReg(LSM330_GYRO_ADDRESS, LSM330_OUT_Z_H, &valueH) )
    {
        return MEMS_ERROR;
    }

    buff->AXIS_Z = (i16_t)( (valueH << 8) | valueL ) / 16;

    return MEMS_SUCCESS;
}

////////////////////////////////////
//            ACC end             //
////////////////////////////////////

////////////////////////////////////
//           Gyro start           //
////////////////////////////////////
void LSM330_Gyro_init()
{
    delayms(100);
    WriteReg(LSM330_GYRO_ADDRESS , 0x20 , 0x8F ); // CTRL_REG1   400Hz ODR, 20hz filter, run!
    delayms(5);
    WriteReg(LSM330_GYRO_ADDRESS , 0x24 , 0x02 ); // CTRL_REG5   low pass filter enable
    delayms(5);
    WriteReg(LSM330_GYRO_ADDRESS , 0x23 , 0x30); // CTRL_REG4 Select 2000dps
}

/*
void Gyro_getADC () {
  i2c_getSixRawADC(LSM330_GYRO_ADDRESS,0x80|0x28);

  GYRO_ORIENTATION( ((rawADC[1]<<8) | rawADC[0])>>2  ,
                    ((rawADC[3]<<8) | rawADC[2])>>2  ,
                    ((rawADC[5]<<8) | rawADC[4])>>2  );
  GYRO_Common();
}
*/

////////////////////////////////////
//            Gyro end            //
////////////////////////////////////

// ************************************************************************************************************
// End Of I2C Gyroscope and Accelerometer LSM330
// ************************************************************************************************************
