/*! \file

\brief HAL I2C settings

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

#ifndef _HAL_I2C_H_
#define _HAL_I2C_H_

#define USU_GPIO_I2C
//#define USU_TWI_I2C

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
//#include "halGpioI2c.h"

void halI2C_Init();
uint8_t halI2C_ReadReg(uint8_t deviceAddr, uint8_t Reg, uint8_t *Data);
uint8_t halI2C_WriteReg(uint8_t deviceAddress, uint8_t WriteAddr, uint8_t Data);
uint8_t halI2C_WriteData(uint8_t deviceAddress, uint8_t *pData, uint8_t len);
char halI2C_ReadData(unsigned char i2cAddress, unsigned char *buffer, unsigned char out_cnt, uint16_t period2);
char halI2C_write_read_Data(unsigned char i2cAddress,
    unsigned char *cmd, unsigned char *buffer,
    unsigned char in_cnt, unsigned char out_cnt,
    uint16_t period1, uint16_t period2);

#ifdef __cplusplus
}
#endif

#endif  //_HAL_I2C_H_
