/**
 * \file lsm303dlhc_test.h
 */

#ifndef _lsm303dlhc_test_H_
#define _lsm303dlhc_test_H_

/*- Types ------------------------------------------------------------------*/

/*- Prototypes -------------------------------------------------------------*/
void lsm303dlhc_init(void);

#endif // _lsm303dlhc_test_H_
