#ifndef DHT_H
#define DHT_H

#ifdef ARDUINO
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

#else

#ifdef __cplusplus
extern "C" {
#endif

#include "sysTypes.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "halGpio.h"
#include "sysTimer.h"
#include "halSleep.h"

#define interrupts()   sei()
#define noInterrupts() cli()

#define INPUT  0
#define OUTPUT 1
#define INPUT_PULLUP 2
#define LOW    0
#define HIGH   1

#define DAT_USE_PD6
//#define DAT_USE_PE6

#ifdef DAT_USE_PD6
HAL_GPIO_PIN(DHT_DAT,       D, 6);
#endif //DAT_USE_PD6

#ifdef DAT_USE_PE6
HAL_GPIO_PIN(DHT_DAT,       E, 6);
#endif //DAT_USE_PE6

inline void InitDHTGpio(void)
{
    HAL_GPIO_DHT_DAT_in();
    HAL_GPIO_DHT_DAT_clr();
}


inline void DeInitDHTGpio(void)
{
  HAL_GPIO_DHT_DAT_out();  //DAT
  HAL_GPIO_DHT_DAT_clr();
}

inline void DHT_DAT_Write(uint8_t val)
{
    if(val == LOW)
    {
        HAL_GPIO_DHT_DAT_out();  //DAT
        HAL_GPIO_DHT_DAT_clr();
    }
    else
    {
        HAL_GPIO_DHT_DAT_out();  //DAT
        HAL_GPIO_DHT_DAT_set();
    }
}

inline void DHT_DAT_Mode(uint8_t mode)
{
    if(mode == INPUT)
    {
        HAL_GPIO_DHT_DAT_in();
        HAL_GPIO_DHT_DAT_clr();
    }
    else if(mode == INPUT_PULLUP)
    {
        HAL_GPIO_DHT_DAT_in();
        HAL_GPIO_DHT_DAT_set();
    }
    else
    {
        HAL_GPIO_DHT_DAT_out();
    }
}

#ifdef DAT_USE_PD6
#define digitalPinToPort(P)    PORTD
#define digitalPinToBitMask(P) (1<<6)
#define portOutputRegister(P)  (&PORTD)
#define portInputRegister(P)  (&PIND)
#endif //DAT_USE_PD6

#ifdef DAT_USE_PE6
#define digitalPinToPort(P)    PORTE
#define digitalPinToBitMask(P) (1<<6)
#define portOutputRegister(P)  (&PORTE)
#define portInputRegister(P)  (&PINE)
#endif //DAT_USE_PE6


#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds(a) ( (a) / clockCyclesPerMicrosecond() )
#define microsecondsToClockCycles(a) ( (a) * clockCyclesPerMicrosecond() )

#define delay delayms
#define delayMicroseconds delayus
#define millis SYS_GetSysTick 

#define micros() 0

#ifdef __cplusplus
}
#endif

#endif

/* DHT library

MIT license
written by Adafruit Industries
*/

// how many timing transitions we need to keep track of. 2 * number bits + extra
#define MAXTIMINGS 85

#define DHT11 11
#define DHT22 22
#define DHT21 21
#define AM2301 21

class DHT {
 private:
  uint8_t _pin, _type;
  #ifdef __AVR
    // Use direct GPIO access on an 8-bit AVR so keep track of the port and bitmask
    // for the digital pin connected to the DHT.  Other platforms will use digitalRead.
    uint8_t _bit, _port;
  #endif
  uint32_t _lastreadtime;
  uint16_t _maxcycles;
  bool _lastresult;

 public:
  DHT(uint8_t pin, uint8_t type);
  void begin(void);
  float readTemperature(bool S=false, bool force=false);
  float convertCtoF(float);
  float convertFtoC(float);
   float computeHeatIndex(float temperature, float percentHumidity, bool isFahrenheit=true);
  float readHumidity(bool force=false);
  bool read(bool force=false);
  uint8_t data[5];

  uint16_t expectPulse(bool level);
};


class InterruptLock {
  public:
   InterruptLock() {
    noInterrupts();
   }
   ~InterruptLock() {
    interrupts();
   }

};

#endif
