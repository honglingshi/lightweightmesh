#include "hal.h"
#include "config.h"
#include "halGpio.h"
#include "Sx1276.h"
#include "halUart.h"
#include "halI2c.h"
#include "halLCD.h"

#ifdef LORA_ON_IOL
HAL_GPIO_PIN(SX1278_DIO0,       E, 0);

HAL_GPIO_PIN(SX1278_MISO,       E, 1);
HAL_GPIO_PIN(SX1278_MOSI,       E, 2);
HAL_GPIO_PIN(SX1278_SCK,        E, 3);
HAL_GPIO_PIN(SX1278_NSS,        E, 4);
HAL_GPIO_PIN(SX1278_RESET,      E, 5);
HAL_GPIO_PIN(SX1278_DIO5,       E, 6);
#else
HAL_GPIO_PIN(SX1278_DIO0,       B, 5);  //IO8

HAL_GPIO_PIN(SX1278_MISO,       B, 1);  //IO13
HAL_GPIO_PIN(SX1278_MOSI,       B, 3);  //IO12
HAL_GPIO_PIN(SX1278_SCK,        B, 2);  //IO11
HAL_GPIO_PIN(SX1278_NSS,        B, 0);  //IO10
HAL_GPIO_PIN(SX1278_RESET,      B, 4);  //IO9
//HAL_GPIO_PIN(SX1278_DIO5,       E, 6);
#endif // LORA_ON_IOL

#define   KFB_LED_OFF()
#define   KFB_LED_TX()
#define   KFB_LED_RX()

#define  SX1278_IO0             HAL_GPIO_SX1278_DIO0_read()

#define  SX1278_SDO             HAL_GPIO_SX1278_MISO_read()
#define  RF_REST_L			    HAL_GPIO_SX1278_RESET_clr()
#define  RF_REST_H			    HAL_GPIO_SX1278_RESET_set()
#define  RF_CE_L                HAL_GPIO_SX1278_NSS_clr()
#define  RF_CE_H                HAL_GPIO_SX1278_NSS_set()
#define  RF_CKL_L               HAL_GPIO_SX1278_SCK_clr()
#define  RF_CKL_H               HAL_GPIO_SX1278_SCK_set()
#define  RF_SDI_L               HAL_GPIO_SX1278_MOSI_clr()
#define  RF_SDI_H               HAL_GPIO_SX1278_MOSI_set()

#define  PA_TXD_OUT()
#define  PA_RXD_OUT()

// uint8_t   si4432_Send[16] = {"www.rf-module.cn"};

void Delay(void)
{
    volatile uint16_t x,y;
    for(x=0; x<1000; x++)
      for(y=0; y<110; y++);
}

// void CLK_init(void)
// {
    // CLK_DeInit();
    // CLK_FastHaltWakeUpCmd (ENABLE );
    // CLK_HSECmd            (DISABLE);
    // CLK_HSICmd            (ENABLE );
    // CLK_SYSCLKConfig      (CLK_PRESCALER_HSIDIV4);
// }

void GPIO_Config(void)
{
    HAL_GPIO_SX1278_MISO_in();
    HAL_GPIO_SX1278_RESET_out();
    HAL_GPIO_SX1278_NSS_out();
    HAL_GPIO_SX1278_SCK_out();
    HAL_GPIO_SX1278_MOSI_out();

    HAL_GPIO_SX1278_MISO_clr();
    HAL_GPIO_SX1278_RESET_set();
    HAL_GPIO_SX1278_NSS_set();
    HAL_GPIO_SX1278_SCK_clr();
    HAL_GPIO_SX1278_MOSI_clr();
}

void SX1276Reset(void)
{
   RF_REST_L;
   Delay1s(200);
   RF_REST_H;
   Delay1s(500);
}

void RF_SPI_MasterIO(unsigned char out)
{
   unsigned char i;
   for (i=0;i<8;i++){
     if (out & 0x80)			/* check if MSB is high */
       RF_SDI_H;
     else RF_SDI_L;							/* if not, set to low */

     RF_CKL_H;						  /* toggle clock high */
     out = (out << 1);					/* shift 1 place for next bit */
     RF_CKL_L;							/* toggle clock low */
   }
}


unsigned char RF_SPI_READ_BYTE()
{
   unsigned char j;
   unsigned char i;
   j=0;
   for (i = 0; i < 8; i++){
     RF_CKL_H;
     j = (j << 1);						 // shift 1 place to the left or shift in 0 //
     if( SX1278_SDO )							 // check to see if bit is high //
       j = j | 0x01; 					   // if high, make bit high //
												  // toggle clock high //
     RF_CKL_L; 							 // toggle clock low //
   }

   return j;								// toggle clock low //
}

void cmdSwitchEn(cmdEntype_t cmd)
{
   switch(cmd)
   {
     case enOpen:{
       RF_CE_L;
     }break;
     case enClose:{
       RF_CE_H;
     }break;
     default:break;
   }
}

void cmdSwitchPA(cmdpaType_t cmd)
{
   switch(cmd)
   {
     case rxOpen:{
       PA_RXD_OUT();
     }break;
     case txOpen:{
       PA_TXD_OUT();
     }break;

     default:break;
   }
}

//void fqcRecvData(unsigned char *lpbuf,unsigned short len)// 接收到RF的数
//{
//   KFB_LED_RX();
//   Delay();Delay();
//   KFB_LED_OFF();
//}
//
extern void fqcRecvData(unsigned char *lpbuf,unsigned short len);
extern void fTransmitFin(void);

HAL_GPIO_PIN(ARD_DIO,       D, 7);

uint8_t g_bSX1278_Inited = 0;

lpCtrlTypefunc_t  ctrlTypefunc = {
   RF_SPI_MasterIO,
   RF_SPI_READ_BYTE,
   cmdSwitchEn,
   cmdSwitchPA,
   fqcRecvData,
   fTransmitFin
};

void App_SX1278_Init(void)
{
//   CLK_init();

   GPIO_Config();

   register_rf_func(&ctrlTypefunc);

   SX1276Reset();

   SX1276LORA_INT();

#if defined(APP_ENDDEVICE)
    RF_SEELP();
#else
    RF_RECEIVE();
#endif // defined(APP_ENDDEVICE)

   g_bSX1278_Inited = 1;
}

void App_SX1278_DeInit(void)
{
    RF_SEELP();

    g_bSX1278_Inited = 0;

#ifdef  RF_ON_OFF_ARDIO
    HAL_GPIO_ARD_DIO_out();
    HAL_GPIO_ARD_DIO_clr();     //RF ARD DIO Off
#endif // RF_ON_OFF_ARDIO
}

void App_SX1278_UartDumpReg(void)
{
    uint8_t i, j;
    for(i=0; i<32; i++)
    {
        for(j=0; j<4; j++)
        {
            uint8_t Val = SX1276ReadBuffer(i+j*32);
            uint8_t Val_H = (Val>>4)&0xf;
            uint8_t Val_L = (Val)&0xf;
            if(Val_H>9)
                HAL_UartWriteByte('A' + Val_H - 10);
            else
                HAL_UartWriteByte('0' + Val_H);
            if(Val_L>9)
                HAL_UartWriteByte('A' + Val_L - 10);
            else
                HAL_UartWriteByte('0' + Val_L);
            HAL_UartWriteByte(' ');
        }
        HAL_UartWriteByte('\r');
        HAL_UartWriteByte('\n');
    }
    HAL_UartWriteByte('\r');
    HAL_UartWriteByte('\n');
}

//#define LORA_PER

void App_SX1278_TX_Test(void)
{
    uint8_t   PER_Buf[16];
    static uint32_t ns_Cnt = 0;
    ns_Cnt ++;

#if !defined(LORA_PER)
    PER_Buf[0] = 1;
#else
    PER_Buf[0] = 0;
#endif // defined(APP_ENDDEVICE)
    PER_Buf[1] = (ns_Cnt >> 24) & 0xff;
    PER_Buf[2] = (ns_Cnt >> 16) & 0xff;
    PER_Buf[3] = (ns_Cnt >> 8) & 0xff;
    PER_Buf[4] = ns_Cnt & 0xff;
#if !defined(LORA_PER)
    PER_Buf[5] = 'E';
    PER_Buf[6] = 'N';
    PER_Buf[7] = 'D';
#else
    PER_Buf[5] = 'P';
    PER_Buf[6] = 'E';
    PER_Buf[7] = 'R';
#endif // defined(APP_ENDDEVICE)
    PER_Buf[8] = 0;

#ifdef  RF_ON_OFF_ARDIO
    HAL_GPIO_ARD_DIO_out();
    HAL_GPIO_ARD_DIO_set();     //RF ARD DIO On
#endif // RF_ON_OFF_ARDIO

    Delay1s(500);

//    halI2C_Init();

    App_SX1278_Init();

    FUN_RF_SENDPACKET(PER_Buf,9);

#ifdef RGB_LCD_TXNUM
    halLCD_Print(ns_Cnt);
#endif // RGB_LCD


//    App_SX1278_DumpReg();
}

uint8_t   SX1278_Reg[128];

void App_SX1278_Test(void)
{
#if defined(APP_ENDDEVICE)
    App_SX1278_TX_Test();
#endif // defined(APP_ENDDEVICE)
}

void App_SX1278_DumpReg(void)
{
    uint8_t i;

    SX1278_Reg[0] = 101;
    for(i=1; i<128; i++)
    {
        SX1278_Reg[i] = SX1276ReadBuffer(i);
    }
}

void App_SX1278_TaskHandle(void)
{
    if (g_bSX1278_Inited)
    {
        //SX1278_Interupt();

        if(SX1278_IO0)
        {
            SX1278_Interupt();
        }
    }
}

//************************以下需要根据不同的单片机进得移植，中断服务函数******************
//INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
//{
//  if(GPIO_ReadInputPin(GPIOC,GPIO_PIN_4)){
 //    SX1278_Interupt();
 // }
//}
//****************************************************************************************/

#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    while (1)
    {
    }
}
#endif
