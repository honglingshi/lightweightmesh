/*! \file

\brief iLive2 driver header

\copyright Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
Republication or redistribution of SMIR Software content is prohibited without the prior written consent of SMIR Team.

\author Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

*/

#ifndef _HAL_HSDTVI_H_
#define _HAL_HSDTVI_H_

//#include <inttypes.h>
//#include <avr/io.h>
//#include <types.h>

//#define HSDTVI_PortE
//#define HSDTVI_PortB
#define NO_HSDTVI

#ifdef NO_HSDTVI
INLINE void  HSDTVI_Init() {}
INLINE void  HSDTVI_OUT_8bit() {}
#endif

#ifdef HSDTVI_PortE

/******************************************************************************
* void HSDTVI_X_set() sets HSDTVI_X pin to logical 1 level.
* void HSDTVI_X_clr() clears HSDTVI_X pin to logical 0 level.
* void HSDTVI_X_make_in makes HSDTVI_X pin as input.
* void HSDTVI_X_make_out makes HSDTVI_X pin as output.
* uint8_t HSDTVI_X_read() returns logical level HSDTVI_X pin.
* uint8_t HSDTVI_X_state() returns configuration of HSDTVI_X port.
*******************************************************************************/
#define HSDTVI_ASSIGN_PIN(name, port, bit) \
    INLINE void  HSDTVI_##name##_set()         {PORT##port |= (1 << bit);} \
    INLINE void  HSDTVI_##name##_clr()         {PORT##port &= ~(1 << bit);} \
    INLINE uint8_t  HSDTVI_##name##_read()     {return (PIN##port & (1 << bit)) != 0;} \
    INLINE uint8_t  HSDTVI_##name##_state()    {return (DDR##port & (1 << bit)) != 0;} \
    INLINE void  HSDTVI_##name##_make_out()    {DDR##port |= (1 << bit);} \
    INLINE void  HSDTVI_##name##_make_in()     {DDR##port &= ~(1 << bit); PORT##port &= ~(1 << bit);} \
    INLINE void  HSDTVI_##name##_make_pullup() {PORT##port |= (1 << bit);}\
    INLINE void  HSDTVI_##name##_toggle()      {PORT##port ^= (1 << bit);}

HSDTVI_ASSIGN_PIN(0, E, 0);
HSDTVI_ASSIGN_PIN(1, E, 1);
HSDTVI_ASSIGN_PIN(2, E, 2);
HSDTVI_ASSIGN_PIN(3, E, 3);
HSDTVI_ASSIGN_PIN(4, E, 4);
HSDTVI_ASSIGN_PIN(5, E, 5);
HSDTVI_ASSIGN_PIN(6, E, 6);
HSDTVI_ASSIGN_PIN(7, E, 7);
HSDTVI_ASSIGN_PIN(WR, G, 2);

// Init HSDTVI_X pin as output.
INLINE void  HSDTVI_Init()
{
    DDRE = 0xFF;
    PORTE = 0xFF;
    //  DDRE |= 0xF8;   //Remove E0~E2
    //  PORTE |= 0xF8;  //Remove E0~E2
    DDRG |= 0x04;
    PORTG |= 0x04;

    volatile unsigned long i = 0;
    for (i = 0; i < 50; i++)
    {
        __asm__ __volatile__ ("nop");
    }
}

/*
#define HSDTVI_OUT_5bit(val) \
{\
    PORTE = (PORTE & 0x07) + (val & 0xF8);  \
    PORTG &= 0xFB;  \
    PORTG |= 0x04;  \
}
#define HSDTVI_OUT_8bit(val)    HSDTVI_OUT_5bit((val)<<3)
*/

#define HSDTVI_OUT_8bit(val) \
    {\
        DDRE = 0xFF;    \
        PORTE = (val);  \
        DDRG |= 0x04;   \
        HSDTVI_WR_toggle(); \
    }
#endif


#ifdef HSDTVI_PortB

/******************************************************************************
* void HSDTVI_X_set() sets HSDTVI_X pin to logical 1 level.
* void HSDTVI_X_clr() clears HSDTVI_X pin to logical 0 level.
* void HSDTVI_X_make_in makes HSDTVI_X pin as input.
* void HSDTVI_X_make_out makes HSDTVI_X pin as output.
* uint8_t HSDTVI_X_read() returns logical level HSDTVI_X pin.
* uint8_t HSDTVI_X_state() returns configuration of HSDTVI_X port.
*******************************************************************************/
#define HSDTVI_ASSIGN_PIN(name, port, bit) \
    INLINE void  HSDTVI_##name##_set()         {PORT##port |= (1 << bit);} \
    INLINE void  HSDTVI_##name##_clr()         {PORT##port &= ~(1 << bit);} \
    INLINE uint8_t  HSDTVI_##name##_read()     {return (PIN##port & (1 << bit)) != 0;} \
    INLINE uint8_t  HSDTVI_##name##_state()    {return (DDR##port & (1 << bit)) != 0;} \
    INLINE void  HSDTVI_##name##_make_out()    {DDR##port |= (1 << bit);} \
    INLINE void  HSDTVI_##name##_make_in()     {DDR##port &= ~(1 << bit); PORT##port &= ~(1 << bit);} \
    INLINE void  HSDTVI_##name##_make_pullup() {PORT##port |= (1 << bit);}\
    INLINE void  HSDTVI_##name##_toggle()      {PORT##port ^= (1 << bit);}

HSDTVI_ASSIGN_PIN(0, B, 0);
HSDTVI_ASSIGN_PIN(1, B, 1);
HSDTVI_ASSIGN_PIN(2, B, 2);
HSDTVI_ASSIGN_PIN(3, B, 3);
HSDTVI_ASSIGN_PIN(4, B, 4);
HSDTVI_ASSIGN_PIN(5, B, 5);
HSDTVI_ASSIGN_PIN(6, B, 6);
HSDTVI_ASSIGN_PIN(7, B, 7);
HSDTVI_ASSIGN_PIN(WR, D, 7);

// Init HSDTVI_X pin as output.
INLINE void  HSDTVI_Init()
{
    DDRB = 0xFF;
    PORTB = 0xFF;

    DDRD |= 0x80;
    PORTD |= 0x80;
}

#define HSDTVI_OUT_8bit(val) \
    {\
        DDRB = 0xFF;    \
        PORTB = (val);  \
        DDRD |= 0x80;   \
        HSDTVI_WR_toggle(); \
    }

#endif


//mod 0--15
//index 0--15
#define HSDTVI_DEFINE_VAL(mod, index) \
    (((mod) << 4) + (index))

#define HSDTVI_MOD_SYS      0x0
#define HSDTVI_MOD_SENSOR   0x1
#define HSDTVI_MOD_SLEEP    0x2
#define HSDTVI_MOD_UART     0x3
#define HSDTVI_MOD_ERR      0xE

//HSDTVI Mod Sys
#define HSDTVI_SYS_START            HSDTVI_DEFINE_VAL(HSDTVI_MOD_SYS, 0)

//HSDTVI Mod Sensor
#define HSDTVI_SENSOR_TSL2500       HSDTVI_DEFINE_VAL(HSDTVI_MOD_SENSOR, 1)
#define HSDTVI_SENSOR_SHT           HSDTVI_DEFINE_VAL(HSDTVI_MOD_SENSOR, 2)
#define HSDTVI_SENSOR_ADC           HSDTVI_DEFINE_VAL(HSDTVI_MOD_SENSOR, 3)
#define HSDTVI_SENSOR_END           HSDTVI_DEFINE_VAL(HSDTVI_MOD_SENSOR, 4)
#define HSDTVI_SENSOR_LM75          HSDTVI_DEFINE_VAL(HSDTVI_MOD_SENSOR, 5)

//HSDTVI Mod Sleep
#define HSDTVI_SLEEP_WAKUP          HSDTVI_DEFINE_VAL(HSDTVI_MOD_SLEEP, 0)
#define HSDTVI_SLEEP_PRE            HSDTVI_DEFINE_VAL(HSDTVI_MOD_SLEEP, 1)

//HSDTVI Mod UART
#define HSDTVI_UART_RX              HSDTVI_DEFINE_VAL(HSDTVI_MOD_UART, 5)
#define HSDTVI_UART_TX              HSDTVI_DEFINE_VAL(HSDTVI_MOD_UART, 10)

//HSDTVI Mod Test
#define HSDTVI_TEST_55              0x55
#define HSDTVI_TEST_AA              0xAA


#endif //_HAL_HSDTVI_H_
