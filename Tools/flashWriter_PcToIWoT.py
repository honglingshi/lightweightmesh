# Copyright: Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
#            Republication or redistribution of SMIR Software content is
#            prohibited without the prior written consent of SMIR Team.

# Author: Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

import os
import atexit
import serial
from time import sleep
import subprocess

try:
    isRasp = True
    import RPi.GPIO as GPIO
    MY_PORT = "/dev/ttyAMA0"

except ImportError:
    isRasp = False
    MY_PORT = "/dev/ttyUSB0"

MY_SERIAL = serial.Serial(MY_PORT, 38400, timeout=0)

MAKE_DIR_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../LwMesh_1_2_1/apps/WSNDemo/make")


def subprocess_cmd(command):
    process = subprocess.Popen(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    proc_stdout = process.communicate()
    return proc_stdout


def build_lw_mesh():

    make_file_path = os.path.join(MAKE_DIR_PATH,
                                  "Makefile_Rcb128rfa1_ATmega128rfa1")

    re = subprocess_cmd("cd \"" + MAKE_DIR_PATH + "\";" +
                        "make -f \"" + make_file_path + "\" clean;" +
                        "make -f \"" + make_file_path + "\" all")

    return re


def builder():
    print "Building Light weight mesh"

    make_result = build_lw_mesh()
    make_result = str(make_result)
    make_result = make_result.replace("\\n", os.linesep)

    print "\n", make_result, "\n"

    if "OBJCOPY Debug/WSNDemo.srec" in make_result:
        print "Build finished successfully"
        return True
    else:
        print "Build failure !"
        return False


def writer():

    # read srec ...
    content = []
    srec_file_path = os.path.join(MAKE_DIR_PATH, "Debug/WSNDemo.srec")

    try:
        f = open(srec_file_path)
        content = f.readlines()
        f.close()
    except:
        print "Read Error (open failure): " + srec_file_path
        return False

    if len(content) <= 0:
        print "Read Error (srec size=0): " + srec_file_path
        return False

    print "\nFound a correct srec file. Flash writer starts.\n"

    # handshake ...
    count = 0
    handshake_req = bytearray([0xB2, 0xA5, 0x65, 0x4B])
    handshake_conf = "69d3d226"  # the manual is WRONG!

    while True:
        s = "Step 1: Preparing handshake. Please unplug the USBSerial cable from your node"
        print s

        answer = raw_input('Step 1: Done? (type "yes" if it is done) ')
        if answer.lower() == "yes" or answer.lower() == "y":
            break

    print "Step 1: You can plug the USBSerial cable back to you node now"

    while True:

        MY_SERIAL.write(handshake_req)

        hex_data = MY_SERIAL.read(50)
        if len(hex_data) > 0 and \
                hex_data.encode("hex").find(handshake_conf) != -1:
            break

        sleep(0.02)

        count += 1
        if count > 100:
            s = "Step 1: Please unplug and replug the USBSerial cable again "
            s += "until you see \"Got handshake confirm\""
            print s
            count = 0

    print "Step 1: Got handshake confirm\n"

    # start to upload ...
    print "Step 2: Writing the srec file to the flash memory on your node"

    count = 0
    a_record = []
    c_ary = []
    for line in content:

        cc = ""
        index = 0
        a_record = []
        c_ary = []
        for c in line:
            # for the first two marker only
            if index == 0 or index == 1:
                MY_SERIAL.write(c)
                index += 1
                continue

            if c == "\r" or c == "\n":
                break

            if cc == "":
                cc = c
            else:
                cc += c
                c_ary.append(cc)
                a_record.append(int(cc, 16))
                cc = ""

        MY_SERIAL.write(bytearray(a_record))

        count += 1
        re = MY_SERIAL.read(100).encode("hex")
        if re.find("2d595ab2") != -1:
            print "Step 2: Writing error on line: " + str(count)
            return False
        else:
            if count % 100 == 0:
                print "Step 2: Wrote " + str(count) + " lines"

        sleep(0.03)

    print "\nFlash writer finished the job\n"

    return True


def exit_handler():
    MY_SERIAL.close()
    print 'Finished clean up'

atexit.register(exit_handler)


def main():
    print "To kill: ctrl+c or sudo kill -9 ", os.getpid(), "\n"

    try:
        if builder():
            sleep(2)
            writer()
    except KeyboardInterrupt:
        exit_handler()

if __name__ == "__main__":
    main()
