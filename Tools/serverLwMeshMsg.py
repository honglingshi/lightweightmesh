#!/usr/bin/python

# Copyright: Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
#            Republication or redistribution of SMIR Software content is
#            prohibited without the prior written consent of SMIR Team.

# Author: Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx


import binascii
import datetime
import MySQLdb
import time
import urllib2
import errno    
import os

def gmkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

SCRIPT_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
LOG_DIR_PATH = os.path.join(SCRIPT_DIR_PATH, "Log")

gmkdir_p(LOG_DIR_PATH)

int8_t = 1
uint8_t = 1
int16_t = 2
uint16_t = 2
int32_t = 4
uint32_t = 4
uint64_t = 8
unknown_t = 20


def do_nothing(val):
    return val


def uint8_to_dec(val):
    return ord(val)


def int8_to_dec(val):
    unsigned = uint8_to_dec(val)
    signed = unsigned - 256 if unsigned > 127 else unsigned
    return signed


def uint64_to_hex_str(val):
    return "0x" + str(binascii.b2a_hex(val)).upper()


def any_r_to_hex_str(val):
    val = val[::-1]  # revered
    return "0x" + binascii.b2a_hex(val)


def convert_battery(val):
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    return(num * 1600 / 1024 * 11 + 360)

def convert_temperature(val):
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    num *= 0.5
    return num

#define SHT1X_TEMP_COEFF 3963	//3.3V for SHT15
#define SHT1X_HUM_COEFF1 (-4)
#define SHT1X_HUM_COEFF2 (405)
#define SHT1X_HUM_COEFF3 (-280)
# RH = C1 + C2*1E-4*SO + C3*1E-8*SO*SO

def convert_sht1x_temperature(val):
    num = ord(val[1]) * 256 + ord(val[0])
    num = (num-3963)/100.0
    return num
    
def convert_sht1x_humidity(val):
    num = ord(val[1]) * 256 + ord(val[0])
    RH = -0.0000028 * num
    RH = (0.0405 + RH) * num
    RH = -4.0 + RH
    return RH

def convert_am2302_temperature(val):
    num = ord(val[1]) * 256 + ord(val[0])
    num = (num)/10.0
    return num
    
def convert_am2302_humidity(val):
    num = ord(val[1]) * 256 + ord(val[0])
    RH = (num)/10.0
    return RH
    
def convert_light(val):
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    num = (num * 1600 / 1024 / 2 / 2)
    return num

def convert_ozone(val):
    RL = 1000.0
    VCC = 5000.0
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    num = (num * 1600 * 5 / 1024.0)  #Voltage
    if num > 100 :
        num = VCC/(num/RL)-RL
    num = num * 100
    return int(num)/100.0 #R_GAS 3K---10ppb  63K----1000ppb

def convert_int16(val):
    num = ord(val[1]) * 256 + ord(val[0])
    # print num
    # num = (num * 1600 / 1024 / 2 / 2)
    if num > 32768 :
        num = num - 65536
    return num


def convert_bmp180_temperature(val):
    temp = ord(val[1]) * 256 + ord(val[0])
    # return str(temp / 10.0) + "Celsius"
    return (temp / 10.0)


def convert_bmp180_pressure(val):
    pressure = ord(val[3]) * 256 * 256 * 256 + ord(val[2]) * 256 * 256 + ord(val[1]) * 256 + ord(val[0])
    altitude = 44330.0 * (1 - pow(pressure / 100.0 / 1013.25, 1.0 / 5.255)) * 100
    # return str(pressure / 100.0) + "hPa " + str(int(altitude) / 100.0) + "m"
    # return (pressure / 100.0, int(altitude) / 100.0)
    return int(altitude) / 100.0


def decode(msg):
    ptime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")  # .%f

    # print "Time: ", ptime, "Size: ", len(msg), ", Raw: ", binascii.b2a_hex(msg)

    pack = [
        {"field": "commandId", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "nodeType", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "extAddr", "length": uint64_t, "method": uint64_to_hex_str, "result": None},

        {"field": "shortAddr", "length": uint16_t, "method": any_r_to_hex_str, "result": None},
        {"field": "softVersion", "length": uint32_t, "method": any_r_to_hex_str, "result": None},
        {"field": "channelMask", "length": uint32_t, "method": any_r_to_hex_str, "result": None},
        {"field": "panId", "length": uint16_t, "method": any_r_to_hex_str, "result": None},
        {"field": "workingChannel", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "parentShortAddr", "length": uint16_t, "method": any_r_to_hex_str, "result": None},

        {"field": "lqi", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "rssi", "length": int8_t, "method": int8_to_dec, "result": None},

        {"field": "boardType", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "sensorSize", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "battery", "length": int32_t, "method": convert_battery, "result": None},

        {"field": "temperature", "length": int32_t, "method": convert_temperature, "result": None},
        {"field": "light", "length": int32_t, "method": convert_light, "result": None},

        {"field": "bmp180_temperature", "length": int16_t, "method": convert_bmp180_temperature, "result": None},
        {"field": "altitude", "length": uint32_t, "method": convert_bmp180_pressure, "result": None},

        {"field": "acc_X", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "acc_Y", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "acc_Z", "length": int16_t, "method": convert_int16, "result": None},

        {"field": "magnetic_X", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "magnetic_Y", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "magnetic_Z", "length": int16_t, "method": convert_int16, "result": None},

        {"field": "acc2_X", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "acc2_Y", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "acc2_Z", "length": int16_t, "method": convert_int16, "result": None},

        {"field": "gyroscope_X", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "gyroscope_Y", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "gyroscope_Z", "length": int16_t, "method": convert_int16, "result": None},

        {"field": "ozone0", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "ozone1", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "ozone2", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "ozone3", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "ozone4", "length": int16_t, "method": convert_int16, "result": None},
        {"field": "ozone5", "length": int16_t, "method": convert_int16, "result": None},

        {"field": "AirTemperature", "length": int16_t, "method": convert_am2302_temperature, "result": None},
        {"field": "AirHumidity", "length": int16_t, "method": convert_am2302_humidity, "result": None},

        {"field": "captionType", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "captionSize", "length": uint8_t, "method": uint8_to_dec, "result": None},
        {"field": "caption", "length": unknown_t, "method": do_nothing, "result": None}
    ]

    index = 0
    caption_size = 0

    bShowHexLog = 0
    bDispFrame = 1
    bLoRaFrame = 0
    bDumpRaw = 1
    bDumpReg = 0
    bLogFrame = 1
    bModemLog = 1
    
    # print "Time:", ptime, " Packet Len = ", len(msg)

    if len(msg) < 3:
        print "Packet Len Error"
        return
    
    if len(msg) > 2:

        hexstr_msg = ":".join("{:02x}".format(ord(c)) for c in msg)
        if bShowHexLog :
            print "Hex Log Time:", ptime, " Len:", len(msg)
            print "=Hex=", hexstr_msg
        # print "=Raw=", msg
        HexLogFileName = "HexLog "+time.strftime('%Y-%m-%d %H')+".txt"
        HexLogFileName = os.path.join(LOG_DIR_PATH, HexLogFileName)
        HexLogFile = open(HexLogFileName,"a")
        HexLogFile.write("Hex Log Time:" + ptime)
        HexLogFile.write(" Len:" + str(len(msg)) + "\r\n")
        HexLogFile.write("=Hex=")
        HexLogFile.write(hexstr_msg)
        HexLogFile.write("\r\n")
        HexLogFile.write("=Raw=")
        HexLogFile.write(msg)
        HexLogFile.write("\r\n")
        HexLogFile.close()

        if ord(msg[0]) is 62 :
            if bModemLog :
                ModemLogFileName = "ModemLog "+time.strftime('%Y-%m-%d %H')+".txt"
                ModemLogFileName = os.path.join(LOG_DIR_PATH, ModemLogFileName)
                ModemLogFile = open(ModemLogFileName,"a")
                ModemLogFile.write("Modem Log Time:" + ptime + "\r\n=M=")
                ModemLogFile.write(msg)
                ModemLogFile.write("\r\n")
                ModemLogFile.close()
            # print "Modem Log Time:", ptime
            # print "=M=", msg
            print msg
            return

        # Change in LoRa Frame
        if ord(msg[0]) is 100 :
            bLoRaFrame = 1
            # bDispFrame = 1
            print "Time:", ptime
            print "LoRaFrame\n"
            
        if ord(msg[0]) is 101 :
            print "Time:", ptime
            if bDumpReg :
                DumpRegFileName = "DumpReg "+time.strftime('%Y-%m-%d %H-%M-%S')+".txt"
                DumpRegFileName = os.path.join(LOG_DIR_PATH, DumpRegFileName)
                DumpRegFile = open(DumpRegFileName,"wb")
                DumpRegFile.write(msg)
                DumpRegFile.close()
            print "DumpReg\n"
            return

        if ord(msg[0]) is 102 :
            if bDumpReg :
                DumpRegFileName = "DumpReg "+time.strftime('%Y-%m-%d %H-%M-%S-Rece')+".txt"
                DumpRegFileName = os.path.join(LOG_DIR_PATH, DumpRegFileName)
                DumpRegFile = open(DumpRegFileName,"wb")
                DumpRegFile.write(msg)
                DumpRegFile.close()
            print "Time:", ptime
            print "Rece LoRa Frame DumpReg\n"
            return

        if ord(msg[0]) is 103 :
            if bDumpReg :
                DumpRegFileName = "DumpReg "+time.strftime('%Y-%m-%d %H-%M-%S-TxFin')+".txt"
                DumpRegFileName = os.path.join(LOG_DIR_PATH, DumpRegFileName)
                DumpRegFile = open(DumpRegFileName,"wb")
                DumpRegFile.write(msg)
                DumpRegFile.close()
            print "Time:", ptime
            print "TxFin LoRa Frame DumpReg\n"
            return

        if ord(msg[0]) is not 1 or len(msg) < 98:
            ModemLogFileName = "ModemLog "+time.strftime('%Y-%m-%d %H')+".txt"
            ModemLogFileName = os.path.join(LOG_DIR_PATH, ModemLogFileName)
            ModemLogFile = open(ModemLogFileName,"a")
            ModemLogFile.write("Frame Len Error\r\n")
            ModemLogFile.close()
            print "Frame Len Error"                                      
            return;

    if bDumpRaw :
        DumpRegFileName = "DumpRaw "+time.strftime('%Y-%m-%d %H')+".txt"
        DumpRegFileName = os.path.join(LOG_DIR_PATH, DumpRegFileName)
        DumpRegFile = open(DumpRegFileName,"a")
        DumpRegFile.write(msg)
        DumpRegFile.close()
    
    for item in pack:
        length = item["length"]
        if caption_size != 0 and item["field"] == "caption":
            length = caption_size

        item["result"] = item["method"](msg[index: index + length])
        index += length

        if item["field"] == "captionSize":
            caption_size = item["result"]


            
        if bLoRaFrame == 1 and item["field"] == "rssi" :
            item["result"] = -164 + uint8_to_dec(msg[index-1])
            
        if bLoRaFrame == 1 and item["field"] == "boardType" :
            item["result"] = -164 + uint8_to_dec(msg[index-1])

        if item["field"] == "shortAddr" and item["result"] == "0x0000":
            bDispFrame = 1
            bLogFrame = 1

        if item["field"] == "shortAddr" and item["result"] == "0x8117":
            bDispFrame = 0
            bLogFrame = 1

    # the output to command-line interface
    cli_decoded_msg = ""
    # the output to graph interface
    gui_decoded_msg = {}
    # the output to log file
    log_msg = ptime
    log_msg_header = "Time"

    url_params = "http://edss.isima.fr/sites/smir/sees/gateIndoor/OzoneFrameInsert?"
    url_params += "timestamp="
    url_params += time.strftime('%Y-%m-%d%%20%H:%M:%S')

    ParmName = ""
    ParmVal = ""
    timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
    
    for item in pack:
        if bDispFrame :
            cli_decoded_msg += (item["field"] + ": " + str(item["result"]) + "\n")
        elif item["field"] is "caption" :
            cli_decoded_msg += (item["field"] + ": " + str(item["result"]) + "\n")
        
        gui_decoded_msg[item["field"]] = item["result"]
        log_msg += ("\t" + str(item["result"]) )
        log_msg_header += ("\t" + item["field"] )
        ParmName += "`"
        if item["field"] is "bmp180_temperature" :
            ParmName += "temperature2"
            url_params += "&temperature2="
        elif item["field"] is "altitude" :
            ParmName += "pressure"
            url_params += "&pressure="
        elif item["field"] is "commandId" :
            ParmName += "messageType"
            url_params += "&messageType="
        elif item["field"] is "sensorSize" :
            ParmName += "sensorsSize"
            url_params += "&sensorsSize="
        elif item["field"] is "captionType" :
            ParmName += "fieldType"
            url_params += "&fieldType="
        elif item["field"] is "captionSize" :
            ParmName += "fieldsize"
            url_params += "&fieldsize="
        elif item["field"] is "panId" :
            ParmName += "panID"
            url_params += "&panID="
        else :
            ParmName += str(item["field"])
            url_params += "&"
            url_params += str(item["field"])
            url_params += "="
        url_params += str(item["result"])
        ParmName += "`, "
        ParmVal  += "'"
        ParmVal  += str(item["result"])
        ParmVal  += "', "
        
    sql_string  = "INSERT INTO ozone_rawframe ("
    sql_string += ParmName
    sql_string += "`timestamp`) VALUES ("
    sql_string += ParmVal
    sql_string += "'"
    sql_string += timestamp
    sql_string += "') "

    # print sql_string

    # try:
        # conn=MySQLdb.connect(host='edss.isima.fr',user='atmouser',passwd='2sZELTA2RQbgEn', charset='utf8')
        # cur=conn.cursor()
        # conn.select_db('atmouser')
        # cur.execute(sql_string)
        # conn.commit()
        # cur.close()
        # conn.close()
     
    # except MySQLdb.Error,e:
         # print "Mysql Error %d: %s" % (e.args[0], e.args[1])

    # print url_params
    
    bUpload = 1
    if bUpload:
        try:
            f = urllib2.urlopen(url_params)
        #    print f.read()
            
        except urllib2.HTTPError,e:
            print e.code
        #    print e.read()
        except urllib2.URLError,e:
            print e.reason
    
    # just print to cli
    print "Time:", ptime
    print cli_decoded_msg
    

    if bLogFrame :
        log_msg += "\n"
        log_msg_header += "\n"
        LogFileName = os.path.join(LOG_DIR_PATH, 'Log.txt')
        f = open(LogFileName,'a')
        f.seek(0,2)
        if f.tell() < 50 :
            f.write(log_msg_header)    
        f.write(log_msg)
        f.close()
            
    # return to gui
    return gui_decoded_msg
