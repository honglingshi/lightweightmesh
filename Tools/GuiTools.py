#test.py
try:
    import tkinter as tk  # for python 3
except:
    import Tkinter as tk  # for python 2
import pygubu
import Tkinter, tkFileDialog
import tkMessageBox
import os
import ntpath
import subprocess as sub
from threading import Thread
import signal
import sys
import sys
import glob
import serial

def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    # sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

uart_cfg = " --port COM13 "
SCRIPT_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
MAKE_INIT_DIR_PATH = os.path.join(SCRIPT_DIR_PATH, "../LwMesh_1_2_1/apps/WSNDemo/make")
g_script_cmd = ""
backtask = None
process = None

Bin_FullPathName = os.path.join(SCRIPT_DIR_PATH, "../LwMesh_1_2_1/apps/WSNDemo/make/Debug/WSNDemo.srec")
Makefile_FullPathName = os.path.join(SCRIPT_DIR_PATH, "../LwMesh_1_2_1/apps/WSNDemo/make/Makefile_Rcb128rfa1_ATmega128rfa1")

class Application:
    def __init__(self, master):

        #1: Create a builder
        self.builder = builder = pygubu.Builder()

        #2: Load an ui file
        SCRIPT_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
        UI_FILE_PATHNAME = os.path.join(SCRIPT_DIR_PATH, 'GuiTools.ui')

        builder.add_from_file(UI_FILE_PATHNAME)

        #3: Create the widget using a master as parent
        self.mainwindow = builder.get_object('GUITools', master)

        # Configure callbacks
        builder.connect_callbacks(self)

        self.RefreshPortListCallBack()
    
    def RefreshPortListCallBack(self):
        strPortList = serial_ports()
        print(strPortList)
        listComPort = self.builder.get_object('listComPort')
        listComPort.delete(0, tk.END)
        for str in strPortList :
            listComPort.insert(tk.END, str)
        listComPort.activate(0)


    def GetConf(self):
        global PrjName, MCUType, Makefile_FullPathName, Bin_FullPathName, uart_cfg

        listComPort = self.builder.get_object('listComPort')
        if listComPort.curselection() :
            strPortName = listComPort.get(listComPort.curselection())
            print("PortName: " + strPortName)
            uart_cfg = " --port " + strPortName

    def BuildCallBack(self):
            self.GetConf()

            if Makefile_FullPathName:
                file_path = Makefile_FullPathName
            else :
                file_path = tkFileDialog.askopenfilename(initialdir = MAKE_INIT_DIR_PATH, filetypes=[("Makefile","*.*")])
            if file_path:
                MAKEFILE_DIR_PATH = os.path.dirname(file_path)
                MAKEFILE_NAME = ntpath.basename(file_path)
                script_cmd = "make -C \"" + MAKEFILE_DIR_PATH + " \" -f \"" + MAKEFILE_NAME + "\" "
                # response = tkMessageBox.showinfo("Build", script_cmd)
                # if response:
                self.RunScript(script_cmd + " all")

    def ReBuildCallBack(self):
        self.GetConf()

        if Makefile_FullPathName:
            file_path = Makefile_FullPathName
        else :
            file_path = tkFileDialog.askopenfilename(initialdir = MAKE_INIT_DIR_PATH, filetypes=[("Makefile","*.*")])
        if file_path:
            MAKEFILE_DIR_PATH = os.path.dirname(file_path)
            MAKEFILE_NAME = ntpath.basename(file_path)
            script_cmd = "make -C \"" + MAKEFILE_DIR_PATH + " \" -f \"" + MAKEFILE_NAME + "\" "
            # response = tkMessageBox.showinfo("ReBuild", script_cmd)
            # if response:
            self.RunScript(script_cmd + " clean all")

    def DownloadCallBack(self):
        self.GetConf()

        if Bin_FullPathName:
            file_path = Bin_FullPathName
        else :
            file_path = tkFileDialog.askopenfilename(initialdir = SCRIPT_DIR_PATH, filetypes=[("Srec files","*.srec")])

        if file_path:
            script_cmd = "python \"" + SCRIPT_DIR_PATH + "\\Download_Firmware.py\" \"" + file_path + "\" " + uart_cfg
            # response = tkMessageBox.askokcancel("Download", script_cmd)
            # if response:
            self.RunScript(script_cmd)

    def MonitorCallBack(self):
        self.GetConf()

        script_cmd = "python \"" + SCRIPT_DIR_PATH + "\\serverCLI.py\" " + uart_cfg
        self.RunScript(script_cmd)

    def StopCallBack(self):
        self.StopScript()

    def CLICmd(self):
        global process
        process = sub.Popen(g_script_cmd)

    def StopScript(self):
        global backtask, process
        if backtask :
            # if backtask.isAlive() :
            backtask.delete()
            backtask = None
        if process:
            process.terminate()
            print "Previous Script Stopped"
            process = None

    def RunScript(self, new_script_cmd):
        global g_script_cmd
        g_script_cmd = new_script_cmd

        self.StopScript()

        backtask = Thread(target=self.CLICmd)
        backtask.start()

if __name__ == '__main__':
    root = tk.Tk()
    app = Application(root)
    root.mainloop()

