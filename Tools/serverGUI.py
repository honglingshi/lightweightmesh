# Copyright: Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
#            Republication or redistribution of SMIR Software content is
#            prohibited without the prior written consent of SMIR Team.

# Author: Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

import os
import atexit
import serverMonitorSerialPort
import StepCount

# import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

# change this if you want to display longer history data
x_items_size = 30

# need to be one of the "APP_ADDR" that you set in config.h
shortAddr_to_draw = "0x0100"

# the field here need to be the same as one in "pack"
# (in decode() of serverLwMesh.py)

field_to_draw1 = "gyroscope_X"
field_to_draw2 = "gyroscope_Y"
field_to_draw3 = "gyroscope_Z"

field_to_draw1 = "acc_X"
field_to_draw2 = "acc_Y"
field_to_draw3 = "acc_Z"

field_to_draw1 = "acc2_X"
field_to_draw2 = "acc2_Y"
field_to_draw3 = "acc2_Z"

field_to_draw1 = "ozone0"
field_to_draw2 = "AirTemperature"
field_to_draw3 = "AirHumidity"

field_to_draw1 = "ozone0"
field_to_draw2 = "ozone1"
field_to_draw3 = "ozone2"
field_to_draw4 = "ozone3"

field_to_draw1 = "acc_X"
field_to_draw2 = "acc_Y"
field_to_draw3 = "acc_Z"
field_to_draw4 = "gyroscope_X"

fig = plt.figure()

ax = plt.axes(xlim=(0, x_items_size), ylim=(-1, 1))
ax.set_title("Node: " + shortAddr_to_draw)
# ax.set_ylabel(field_to_draw)

line1, = ax.plot([], [], lw=2)
line2, = ax.plot([], [], lw=2)
line3, = ax.plot([], [], lw=2)
line4, = ax.plot([], [], lw=2)

step_text = ax.text(x_items_size/2-10, .5, '', fontsize=25, va='center')

plt.legend([field_to_draw1, field_to_draw2, field_to_draw3, field_to_draw4], loc='upper left')

x_items = list(range(x_items_size))
y_items1 = [0] * x_items_size
y_items2 = [0] * x_items_size
y_items3 = [0] * x_items_size
y_items4 = [0] * x_items_size

has_init = False


def init():
    line1.set_data([], [])
    line2.set_data([], [])
    line3.set_data([], [])
    line4.set_data([], [])
    return line1, line2, line3, line4, 


def animate(i):
    global has_init
    global y_items1
    global y_items2
    global y_items3
    global y_items4

    serverMonitorSerialPort.read_once()
    gui_decoded_msgs = serverMonitorSerialPort.unpack_frame()

    for item in gui_decoded_msgs:
        if item["shortAddr"] == shortAddr_to_draw:
            StepCount.DetectorInput(item[field_to_draw1])
            if has_init:
                del y_items1[0]
                del y_items2[0]
                del y_items3[0]
                del y_items4[0]
                y_items1.append(item[field_to_draw1])
                y_items2.append(item[field_to_draw2])
                y_items3.append(item[field_to_draw3])
                y_items4.append(item[field_to_draw4])
            else:
                y_items1 = [item[field_to_draw1]] * x_items_size
                y_items2 = [item[field_to_draw2]] * x_items_size
                y_items3 = [item[field_to_draw3]] * x_items_size
                y_items4 = [item[field_to_draw4]] * x_items_size
                has_init = True

    ymin = min(min(y_items1), min(y_items2), min(y_items3), min(y_items4)) - 3
    ymax = max(max(y_items1), max(y_items2), max(y_items3), max(y_items4)) + 3
    ax.set_ylim(ymin, ymax)

    line1.set_data(x_items, y_items1)
    line2.set_data(x_items, y_items2)
    line3.set_data(x_items, y_items3)
    line4.set_data(x_items, y_items4)

    step_text.set_y( (ymin+ymax)/2 )
#    step_text.set_text("Total:" + str(StepCount.SC_TotalStepCnt) + "  Recent:" + str(StepCount.SC_RecentStepCnt))
	
    return line1, line2, line3, line4, step_text,


def start_animation():

    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=200, interval=10, blit=False)

    plt.show()

    # shut down the serial port after the user closes the window
    serverMonitorSerialPort.shut_down()


def exit_handler():
    serverMonitorSerialPort.shut_down()
    print 'Finished clean up'

atexit.register(exit_handler)


def main():
    print "To kill: close window, or sudo kill -9 ", os.getpid()

    # the loop has been done in animate()
    serverMonitorSerialPort.stop_loop()

    start_animation()

if __name__ == "__main__":
    main()
