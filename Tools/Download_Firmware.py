# Copyright: Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
#            Republication or redistribution of SMIR Software content is
#            prohibited without the prior written consent of SMIR Team.

# Author: Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

import os
import atexit
import argparse
from time import sleep
import platform
import serial

parser = argparse.ArgumentParser(description='Download srec firmware to MCU.')
parser.add_argument('--port', metavar='Port', help='serial port')
parser.add_argument('filename', metavar='filename.srec', help='srec firmware filename')
# parser.print_help()
args = parser.parse_args()
# print(args.filename)
# print(args.port)

if(args.port == None):
    try:
        isRasp = True
        import RPi.GPIO as GPIO
        MY_PORT = "/dev/ttyAMA0"

    except ImportError:
        isRasp = False
        if platform.system() == "Windows":
            MY_PORT = "COM13"
        else:
            MY_PORT = "/dev/ttyUSB0"
else:
    MY_PORT = args.port
    
MY_SERIAL = serial.Serial(MY_PORT, 38400, timeout=0)
MY_SERIAL.close()
MY_SERIAL.setDTR(False)
sleep(0.5)
MY_SERIAL.open()

def writer():

    # read srec ...
    content = []
    srec_file_path = args.filename

    try:
        f = open(srec_file_path)
        content = f.readlines()
        f.close()
    except:
        print "Read Error (open failure): " + srec_file_path
        return False

    if len(content) <= 0:
        print "Read Error (srec size=0): " + srec_file_path
        return False

    print "Flash writer starts ......\n"

    # handshake ...
    count = 0
    handshake_req = bytearray([0xB2, 0xA5, 0x65, 0x4B])
    handshake_conf = "69d3d226"  # the manual is WRONG!

    print "If download didn't start automatically, please click the RESET button on your board"

    while True:

        MY_SERIAL.write(handshake_req)

        hex_data = MY_SERIAL.read(50)
        if len(hex_data) > 0 and \
                hex_data.encode("hex").find(handshake_conf) != -1:
            break

        sleep(0.02)

        count += 1
        if count > 200:
            print "Please make sure you have clicked the RESET button !"
            count = 0

    # print "\nGot a handshake confirm"

    # start to upload ...
    print "Writing to the flash memory"

    count = 0
    a_record = []
    c_ary = []
    for line in content:

        cc = ""
        index = 0
        a_record = []
        c_ary = []
        for c in line:
            # for the first two marker only
            if index == 0 or index == 1:
                MY_SERIAL.write(c)
                index += 1
                continue

            if c == "\r" or c == "\n":
                break

            if cc == "":
                cc = c
            else:
                cc += c
                c_ary.append(cc)
                a_record.append(int(cc, 16))
                cc = ""

        MY_SERIAL.write(bytearray(a_record))

        count += 1
        re = MY_SERIAL.read(100).encode("hex")
        if re.find("2d595ab2") != -1:
            print "Writing error on line: " + str(count)
            return False
        else:
            if count % 100 == 0:
                print "Wrote " + str(count) + " lines"

        sleep(0.03)

    print "\n...... Flash writer finished the writing\n"

    return True


def exit_handler():
    MY_SERIAL.close()
    print 'Finished clean up'

atexit.register(exit_handler)


def main():
    print "To kill: ctrl+c or sudo kill -9 ", os.getpid(), "\n"

    try:
        writer()

    except KeyboardInterrupt:
        exit_handler()

if __name__ == "__main__":
    main()
