-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 26 Avril 2016 à 01:25
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `usu`
--

-- --------------------------------------------------------

--
-- Structure de la table `ozone_rawframe`
--

CREATE TABLE IF NOT EXISTS `ozone_rawframe` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `messageType` varchar(32) NOT NULL,
  `nodeType` varchar(32) NOT NULL,
  `extAddr` varchar(32) NOT NULL,
  `shortAddr` varchar(32) NOT NULL,
  `softVersion` varchar(32) NOT NULL,
  `channelMask` varchar(32) NOT NULL,
  `panID` varchar(32) NOT NULL,
  `workingChannel` varchar(32) NOT NULL,
  `parentShortAddr` varchar(32) NOT NULL,
  `lqi` varchar(32) NOT NULL,
  `rssi` varchar(32) NOT NULL,
  `boardType` varchar(32) NOT NULL,
  `sensorsSize` varchar(32) NOT NULL,
  `battery` varchar(32) NOT NULL,
  `temperature` varchar(32) NOT NULL,
  `light` varchar(32) NOT NULL,
  `pressure` varchar(32) NOT NULL,
  `temperature2` varchar(32) NOT NULL,
  `acc_X` varchar(32) NOT NULL,
  `acc_Y` varchar(32) NOT NULL,
  `acc_Z` varchar(32) NOT NULL,
  `magnetic_X` varchar(32) NOT NULL,
  `magnetic_Y` varchar(32) NOT NULL,
  `magnetic_Z` varchar(32) NOT NULL,
  `acc2_X` varchar(32) NOT NULL,
  `acc2_Y` varchar(32) NOT NULL,
  `acc2_Z` varchar(32) NOT NULL,
  `gyroscope_X` varchar(32) NOT NULL,
  `gyroscope_Y` varchar(32) NOT NULL,
  `gyroscope_Z` varchar(32) NOT NULL,
  `ozone0` varchar(32) NOT NULL,
  `ozone1` varchar(32) NOT NULL,
  `ozone2` varchar(32) NOT NULL,
  `ozone3` varchar(32) NOT NULL,
  `ozone4` varchar(32) NOT NULL,
  `ozone5` varchar(32) NOT NULL,
  `AirTemperature` varchar(32) NOT NULL,
  `AirHumidity` varchar(32) NOT NULL,
  `fieldType` varchar(32) NOT NULL,
  `fieldsize` varchar(32) NOT NULL,
  `caption` varchar(32) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `ExtAdd` (`extAddr`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=448547 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
