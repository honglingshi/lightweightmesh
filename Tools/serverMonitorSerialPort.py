# Copyright: Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
#            Republication or redistribution of SMIR Software content is
#            prohibited without the prior written consent of SMIR Team.

# Author: Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

import serial
from threading import Thread
import threading
# import datetime
from time import sleep
# import struct
# import requests
# import json
import binascii
import serverLwMeshMsg
import platform
import sys

MY_SERIAL = None

Is_Continue_Serail_Read = True
Serial_Raw_CH_Slot_List = []
# serial_raw_chars = ""

MAX_FRAME_SIZE = 200  # to-do: check

APP_CMD_UART_STATE_IDLE = 1
APP_CMD_UART_STATE_SYNC = 2
APP_CMD_UART_STATE_DATA = 3
APP_CMD_UART_STATE_MARK = 4
APP_CMD_UART_STATE_CSUM = 5

Frame_State = APP_CMD_UART_STATE_IDLE
Frame_Sum = 0
Last_Message = ""

def setup(portname):
    global MY_SERIAL
    MY_PORT = portname

    if(MY_PORT is None) :
        try:
            # isRasp = True
            import RPi.GPIO as GPIO
            MY_PORT = "/dev/ttyAMA0"

        except ImportError:
            # isRasp = False
            if platform.system() == "Windows":
                MY_PORT = "COM13"
            else:
                MY_PORT = "/dev/ttyUSB0"

    MY_SERIAL = serial.Serial(MY_PORT, 38400, timeout=0)


def unpack_frame():

    gui_decoded_msgs = []

    global Serial_Raw_CH_Slot_List
    global Last_Message
    global Frame_State
    global Frame_Sum

    serial_raw_chars = ""

    size = len(Serial_Raw_CH_Slot_List)
    while size > 0:
        serial_raw_chars += Serial_Raw_CH_Slot_List.pop(0)
        size -= 1

    for ch in serial_raw_chars:

        ch_ord = ord(ch)

        if Frame_State is APP_CMD_UART_STATE_IDLE:

            if ch_ord is 0x10:
                Last_Message = ""
                Frame_Sum = ch_ord
                Frame_State = APP_CMD_UART_STATE_SYNC

        elif Frame_State is APP_CMD_UART_STATE_SYNC:

            Frame_Sum += ch_ord
            if ch_ord is 0x02:
                Frame_State = APP_CMD_UART_STATE_DATA
            else:
                Frame_State = APP_CMD_UART_STATE_IDLE

        elif Frame_State is APP_CMD_UART_STATE_DATA:

            Frame_Sum += ch_ord
            if ch_ord is 0x10:
                Frame_State = APP_CMD_UART_STATE_MARK
            else:
                Last_Message += ch

            if len(Last_Message) > MAX_FRAME_SIZE:
                Frame_State = APP_CMD_UART_STATE_IDLE

        elif Frame_State is APP_CMD_UART_STATE_MARK:

            Frame_Sum += ch_ord
            if ch_ord is 0x10:
                Last_Message += ch
                if len(Last_Message) > MAX_FRAME_SIZE:
                    Frame_State = APP_CMD_UART_STATE_IDLE
                else:
                    Frame_State = APP_CMD_UART_STATE_DATA
            elif ch_ord is 0x03:
                Frame_State = APP_CMD_UART_STATE_CSUM
            else:
                Frame_State = APP_CMD_UART_STATE_IDLE

        elif Frame_State is APP_CMD_UART_STATE_CSUM:

            if ch_ord is (Frame_Sum % 256):
                gui_decoded_msgs.append(serverLwMeshMsg.decode(Last_Message))

            Frame_State = APP_CMD_UART_STATE_IDLE

    if Is_Continue_Serail_Read:
        threading.Timer(1, unpack_frame).start()

    return gui_decoded_msgs


def write(byte):
    MY_SERIAL.write(byte)


def read(len=1):
    return MY_SERIAL.read(len)


def read_once():

    global Serial_Raw_CH_Slot_List
    data = MY_SERIAL.read(500)
    if len(data) > 0:
        Serial_Raw_CH_Slot_List.append(data)
        data = binascii.b2a_hex(data)  # .decode('ascii')
        print data


def read_loop():
    global Serial_Raw_CH_Slot_List
    global Is_Continue_Serail_Read
    while Is_Continue_Serail_Read:
        data = MY_SERIAL.read(50)
        if len(data) > 0:
            Serial_Raw_CH_Slot_List.append(data)
            # data = binascii.b2a_hex(data)  # .decode('ascii')
            # print data
        sleep(0.01)


def start_loop():

    print "SP > ", "Start Serial Port"
    global Is_Continue_Serail_Read
    Is_Continue_Serail_Read = True

    # out
    unpack_frame()

    # in
    st = Thread(target=read_loop)
    st.daemon = True
    st.start()


def stop_loop():
    global Is_Continue_Serail_Read
    Is_Continue_Serail_Read = False


def shut_down():
    stop_loop()
    if MY_SERIAL :
        MY_SERIAL.close()

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        exit("Too less arguments calling script")
    setup(sys.argv[1])