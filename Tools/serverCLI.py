#!/usr/bin/python

# Copyright: Copyright (C) 2015 SMIR Team / LIMOS / UBP. All rights reserved.
#            Republication or redistribution of SMIR Software content is
#            prohibited without the prior written consent of SMIR Team.

# Author: Kun Mean Hou, Xunxing Diao, Hongling Shi et Christophe de Vaulx

import os
import atexit
from time import sleep
import argparse
import serverMonitorSerialPort

parser = argparse.ArgumentParser(description='Monitor Seiral output from Board.')
parser.add_argument('--port', metavar='Port', help='serial port')
# parser.print_help()
args = parser.parse_args()
MY_PORT = args.port

def exit_handler():
    serverMonitorSerialPort.shut_down()
    print 'Finished clean up'

atexit.register(exit_handler)


def main():
    print "To kill: ctrl+c or sudo kill -9 ", os.getpid()
    serverMonitorSerialPort.setup(MY_PORT)
    serverMonitorSerialPort.start_loop()
    try:
        while True:
            sleep(1)
    except KeyboardInterrupt:
        exit_handler()

if __name__ == "__main__":
    main()
