
global SC_State
global SC_TotalCnt
global SC_RecentCnt
global SC_LowPassOut
global SC_InputCnt
global SC_InputBuf
global SC_IncTopVal
global SC_DecBottomVal

SC_STATES = ['IDLE', 'INC', 'DEC']
SC_BufSize			= 300

SC_Min_Peak2Peak	= 400
SC_Min_Diff			= 200
SC_DectWinSize		= 3

SC_State 			= 'IDLE'
SC_TotalStepCnt		= 0
SC_RecentStepCnt	= 0
SC_LowPassOut		= 0
SC_InputCnt			= 0
SC_InputBuf			= [0] * SC_BufSize

# This class provides the functionality we want. You only need to look at
# this if you want to know how this works. It only needs to be defined
# once, no need to muck around with its internals.
class switch(object):
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration
    
    def match(self, *args):
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True
        elif self.value in args: # changed for v1.5, see below
            self.fall = True
            return True
        else:
            return False

def DetectorInput(xin):
	global SC_State
	global SC_TotalStepCnt
	global SC_RecentStepCnt
	global SC_LowPassOut
	global SC_InputCnt
	global SC_InputBuf
	global SC_IncTopVal
	global SC_DecBottomVal
	
	if SC_InputCnt > 0 :
		SC_LowPassOut = xin #0.95*SC_LowPassOut + 0.05*xin;
		del SC_InputBuf[0]
		SC_InputBuf.append(SC_LowPassOut)
	else :
		SC_LowPassOut = xin
		SC_IncTopVal = xin
		SC_DecBottomVal = xin
		SC_InputBuf = [xin] * SC_BufSize
	SC_InputCnt = SC_InputCnt + 1
	
	for case in switch(SC_State):
		if case('IDLE'):
			if ( (max(SC_InputBuf) - min(SC_InputBuf)) > SC_Min_Peak2Peak ) :
				if (SC_LowPassOut > max(SC_InputBuf[SC_BufSize-SC_DectWinSize:SC_BufSize-1])) :
					SC_IncTopVal = SC_LowPassOut
					SC_State = 'INC'
				elif (SC_LowPassOut < min(SC_InputBuf[SC_BufSize-SC_DectWinSize:SC_BufSize-1])) :
					SC_DecBottomVal = SC_LowPassOut
					SC_State = 'DEC'
			break
		if case('INC'):
			if ( (max(SC_InputBuf) - min(SC_InputBuf)) > SC_Min_Peak2Peak ) :
				if (SC_LowPassOut > max(SC_InputBuf[SC_BufSize-SC_DectWinSize:SC_BufSize-1])) :
					SC_IncTopVal = SC_LowPassOut
					SC_State = 'INC'
				elif (SC_LowPassOut < min(SC_InputBuf[SC_BufSize-SC_DectWinSize:SC_BufSize-1])) :
					if (SC_LowPassOut < SC_IncTopVal - SC_Min_Diff) :
						SC_DecBottomVal = SC_LowPassOut
						SC_TotalStepCnt = SC_TotalStepCnt + 1
						SC_RecentStepCnt = SC_RecentStepCnt + 1
						SC_State = 'DEC'
			else:
				SC_RecentStepCnt = 0
				SC_State = 'IDLE'
			break
		if case('DEC'):
			if ( (max(SC_InputBuf) - min(SC_InputBuf)) > SC_Min_Peak2Peak ) :
				if (SC_LowPassOut < min(SC_InputBuf[SC_BufSize-SC_DectWinSize:SC_BufSize-1])) :
					SC_DecBottomVal = SC_LowPassOut
					SC_State = 'DEC'
				elif (SC_LowPassOut > max(SC_InputBuf[SC_BufSize-SC_DectWinSize:SC_BufSize-1])) :
					if (SC_LowPassOut > SC_DecBottomVal + SC_Min_Diff) :
						SC_IncTopVal = SC_LowPassOut
						SC_TotalStepCnt = SC_TotalStepCnt + 1
						SC_RecentStepCnt = SC_RecentStepCnt + 1
						SC_State = 'INC'
			else:
				SC_RecentStepCnt = 0
				SC_State = 'IDLE'
			break
		if case(): # default
			print "Error"
			SC_TotalStepCnt		= 0
			SC_RecentStepCnt	= 0
			SC_LowPassOut		= 0
			SC_InputCnt			= 0
			SC_InputBuf			= [0] * SC_BufSize

